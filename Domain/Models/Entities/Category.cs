using System;
using System.Collections.Generic;

namespace Dermi.Domain.Models.Entities
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
