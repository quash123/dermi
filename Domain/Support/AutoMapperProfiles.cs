using System.Linq;
using AutoMapper;
using Dermi.Domain.Dtos;
using Dermi.Domain.Models;
using Dermi.Domain.Models.Entities;


namespace Dermi.Domain.Support {
    public class AutoMapperProfiles : Profile 
    {
        public AutoMapperProfiles () 
        {
            CreateMap<Category, CategoryForCreationDto> ().ReverseMap ();
            CreateMap<Category, CategoryToReturnDto> ().ReverseMap ();
            CreateMap<Category, CategoryForUpdateDto> ().ReverseMap ();

            CreateMap<ProductForCreationDto, Product> ();
            CreateMap<Product, ProductToReturnDto>()
            .ForMember(p => p.CategoryId, opt => opt.MapFrom( s => s.Category.Id));

            CreateMap<User, UserToReturnDto>();


            CreateMap<News, NewsForCreationDto> ().ReverseMap ();
            CreateMap<News, NewsForUpdateDto> ().ReverseMap ();
            CreateMap<News, NewsToReturnDto> ().ReverseMap ();
        }
    }
}