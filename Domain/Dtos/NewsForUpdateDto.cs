using System;

namespace Dermi.Domain.Dtos
{
    public class NewsForUpdateDto
    {
        public int Id {get; set;}
        public DateTime Added {get; set;}
        public string Title {get; set;}
        public string Html {get; set;}
        public string PhotoUrl {get; set;}
    }
}