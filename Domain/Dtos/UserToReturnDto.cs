namespace Dermi.Domain.Dtos
{
    public class UserToReturnDto
    {
        public string Username {get; set;}
        public string Token {get; set;}
    }
}