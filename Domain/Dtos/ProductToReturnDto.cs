namespace Dermi.Domain.Dtos
{
    public class ProductToReturnDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set;}
        public decimal? Price { get; set; }
    }
}