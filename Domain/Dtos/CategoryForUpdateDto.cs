namespace Dermi.Domain.Dtos
{
    public class CategoryForUpdateDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}