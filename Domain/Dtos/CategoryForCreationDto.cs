namespace Dermi.Domain.Dtos 
{
    public class CategoryForCreationDto 
    {
        public string Name { get; set; }
    }
}