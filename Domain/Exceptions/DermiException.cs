﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Dermi.Domain.Exceptions
{
    public class DermiException : Exception
    {
        public int StatusCode { get; set; }
        public string ClientMessage { get; set; }
        public string ErrorCodeName { get; set; }

        public DermiException()
        {
        }

        public DermiException(string clientMessage, string errroCodeName, int statusCode)
        {
            ClientMessage = clientMessage;
            ErrorCodeName = errroCodeName;
            StatusCode = statusCode;
        }

        
    }
}
