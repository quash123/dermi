using System;
using System.Net;
using Dermi.Domain.Exceptions;
using Dermi.Extensions;
using Infrastructure.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Serilog;

namespace Dermi.Filters
{
[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class CustomExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            var code = HttpStatusCode.InternalServerError;
            if (context.Exception is ValidationException)
            {
                context.HttpContext.Response.ContentType = "application/json";
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                context.Result = new JsonResult(
                    ((ValidationException)context.Exception).Failures);

                return;
            }  else if (context.Exception is DermiException) {
                DermiException ex = (DermiException) context.Exception;
                context.HttpContext.Response.ContentType = "application/json";
                context.HttpContext.Response.StatusCode = ex.StatusCode;
                context.Result = new JsonResult(new
                {
                    error = ex.ClientMessage, errroCode = ex.ErrorCodeName
                });
                if (ex.StatusCode == 500) Log.Error (ex.FormatExceptionToString());    
            }
            else {
                context.HttpContext.Response.ContentType = "application/json";
                context.HttpContext.Response.StatusCode = (int)code;
                context.Result = new JsonResult(new
                {
                    error = new[] { context.Exception.Message },
                    stackTrace = context.Exception.StackTrace
                });
            }

            // if (context.Exception is NotFoundException)
            // {
            //     code = HttpStatusCode.NotFound;
            // }

            
        }
    }
}