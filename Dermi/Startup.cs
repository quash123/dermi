using System.Collections.Generic;
using System.Net;
using System.Reflection;
using System.Text;
using AutoMapper;
using Dermi.Exceptions;
using Dermi.Filters;
using Dermi.Infrastructure.Data;
using FluentValidation.AspNetCore;
using Infrastructure.Commands;
using Infrastructure.Queries.Categories;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using Swashbuckle.AspNetCore.Swagger;

namespace Dermi {
    public class Startup {
        public Startup (IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices (IServiceCollection services) {
            services.AddDbContext<DataContext> (x => x.UseSqlServer (Configuration.GetConnectionString ("DefaultConnection")));
            services.AddTransient<Seed> ();
            services.AddMediatR (typeof (CreateCategoryCommand).GetTypeInfo ().Assembly);

            services.AddMvc (options => options.Filters.Add (typeof (CustomExceptionFilterAttribute)))
                .SetCompatibilityVersion (CompatibilityVersion.Version_2_1)
                .AddFluentValidation (fv => fv.RegisterValidatorsFromAssemblyContaining<GetCategoriesQueryValidator> ())
                .AddJsonOptions (opt => {
                    opt.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                });

            services.AddCors ();
            services.AddSwaggerGen (c => {
                c.SwaggerDoc ("v1", new Info { Title = "Dermi API", Version = "v1" });

                c.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
                var security = new Dictionary<string, IEnumerable<string>>
                {
                    {"Bearer", new string[] { }},
                };
                c.AddSecurityRequirement(security);
            });
            services.AddAuthentication (JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer (options => {
                    options.TokenValidationParameters = new TokenValidationParameters {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey (Encoding.ASCII
                    .GetBytes (Configuration.GetSection ("AppSettings:Token").Value)),
                    ValidateIssuer = false,
                    ValidateAudience = false
                    };
                });

            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles (configuration => {
                configuration.RootPath = "ClientApp/dist";
            });

            services.AddAutoMapper ();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure (IApplicationBuilder app, IHostingEnvironment env, Seed seed) {
            // if (env.IsDevelopment())
            // {
            //     app.UseDeveloperExceptionPage();
            // }
            // else
            // {
            //     app.UseExceptionHandler("/Error");
            //     ///app.UseHsts();
            // }
            // app.UseExceptionHandler (builder => {
            //     builder.Run (async context => {
            //         context.Response.StatusCode = (int) HttpStatusCode.InternalServerError;

            //         var error = context.Features.Get<IExceptionHandlerFeature> ();
            //         if (error != null) {
            //             Log.Error(error.Error.Message);
            //             if (error.Error  is DermiException)
            //             {
            //                 context.Response.StatusCode = ((DermiException)error.Error).StatusCode;
            //                 context.Response.AddApplicationError (((DermiException)error.Error).ClientMessage);
            //                 await context.Response.WriteAsync (((DermiException)error.Error).ClientMessage);
            //             } else {
            //                 context.Response.AddApplicationError (error.Error.Message);
            //                 await context.Response.WriteAsync (error.Error.Message);
            //             }

            //         }
            //     });
            // });
            //app.ConfigureExceptionHandler ();
            //app.UseHttpsRedirection();

            app.UseSwagger ();
            app.UseSwaggerUI (c => {
                c.SwaggerEndpoint ("/swagger/v1/swagger.json", "Dermi API V1");
            });

            app.UseCors (x => x.AllowAnyOrigin ().AllowAnyMethod ().AllowAnyHeader ());
            app.UseAuthentication ();
            app.UseMvc (routes => {
                routes.MapRoute (
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });
            app.UseStaticFiles ();
            app.UseSpaStaticFiles ();
            app.UseSpa (spa => {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ClientApp";

                // if (env.IsDevelopment ()) {
                //     spa.UseAngularCliServer (npmScript: "start");
                // }
            });

            seed.SeedDb ().Wait ();
        }
    }
}