using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using Dermi.Exceptions;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using MediatR;
using Infrastructure.Queries;
using Dermi.Domain.Dtos;
using Infrastructure.Commands;
using System.Net;
using Dermi.Domain.Models;
using Dermi.Extensions;


namespace Dermi.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase {
        private readonly IMediator _mediator;

        public AuthController (IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegisterCommand command)
        {
            var user = await _mediator.Send(command);
            return Ok(user);
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] LoginCommand command)
        {
            var user = await _mediator.Send(command);
            return Ok(user);
        }
    }
}