﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using Dermi.Exceptions;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using MediatR;
using Infrastructure.Queries;
using Dermi.Domain.Dtos;
using Infrastructure.Commands;
using System.Net;
using Dermi.Domain.Models;
using Dermi.Extensions;

namespace Dermi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IMediator _mediator;


        public ProductController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet()]
        [ProducesResponseType(typeof(PagedList<ProductToReturnDto>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetPage([FromQuery] GetProductsQuery command)
        {
            var results = await _mediator.Send(command);
            return Ok (results);
        }

        [HttpGet(Name = "GetProduct")]
        [ProducesResponseType(typeof(ProductToReturnDto), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get([FromQuery] GetProductQuery command)
        {
            var result = await _mediator.Send(command);
            if (result == null) return NotFound();
            return Ok (result);
        }
      
        [HttpPost()]
        [Authorize]
        [ProducesResponseType(typeof(ProductToReturnDto), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> Add([FromBody] CreateProductCommand command)
        {
            var result = await _mediator.Send(command);
            return CreatedAtRoute ("GetProduct", new { id = result.Id }, result);
        }

        [HttpPut()]
        [Authorize]
        [ProducesResponseType(typeof(ProductToReturnDto), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update([FromBody] UpdateProductCommand command)
        {
            var result = await _mediator.Send(command);    
            return Ok(result);
        }

        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> Delete(DeleteProductCommand command)
        {
            await _mediator.Send(command);
            return NoContent();
        }

    }
}