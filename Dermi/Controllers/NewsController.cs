using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using Dermi.Exceptions;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using MediatR;
using Infrastructure.Queries;
using Dermi.Domain.Dtos;
using Infrastructure.Commands;
using System.Net;
using Dermi.Domain.Models;
using Dermi.Extensions;

namespace Dermi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NewsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public NewsController(IMediator mediator)
        {
            _mediator = mediator;
        }


        [HttpGet()]
        [ProducesResponseType(typeof(PagedList<NewsToReturnDto>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetPage([FromQuery] GetNewsQuery command)
        {
            var results = await _mediator.Send(command);
            return Ok (results);
        }
        
        [HttpGet(Name = "GetSingleNews")]
        [ProducesResponseType(typeof(NewsToReturnDto), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get([FromQuery] GetSingleNewsQuery command)
        {
            var result = await _mediator.Send(command);
            if (result == null) return NotFound();
            return Ok (result);
        }
      

        [HttpPost()]
        [Authorize]
        [ProducesResponseType(typeof(NewsToReturnDto), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> Add([FromBody] CreateNewsCommand command)
        {
            var result = await _mediator.Send(command);
            return CreatedAtRoute ("GetSingleNews", new { id = result.Id }, result);
        }

        [HttpPut()]
        [Authorize]
        [ProducesResponseType(typeof(NewsToReturnDto), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update([FromBody] UpdateNewsCommand command)
        {
            var result = await _mediator.Send(command);    
            return Ok(result);
        }

        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> Delete(DeleteNewsCommand command)
        {
            await _mediator.Send(command);
            return NoContent();
        }

    }
}