using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using Dermi.Exceptions;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using MediatR;
using Infrastructure.Queries;
using Dermi.Domain.Dtos;
using Infrastructure.Commands;
using System.Net;
using Dermi.Domain.Models;
using Dermi.Extensions;
using Infrastructure.Queries.Categories;
using Infrastructure.Exceptions;

namespace Dermi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CategoriesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("GetPage")]
        [ProducesResponseType(typeof(PagedList<CategoryToReturnDto>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetPage([FromQuery] GetCategoriesQuery command)
        {
            var results = await _mediator.Send(command);
            return Ok (results);
        }

        [HttpGet("", Name = "GetCategory")]
        [ProducesResponseType(typeof(CategoryToReturnDto), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get([FromQuery] GetCategoryQuery command)
        {
            var results = await _mediator.Send(command);
            if (results == null) return NotFound();
            return Ok (results);
        }
      
        [HttpPost()]
        [Authorize]
        [ProducesResponseType(typeof(CategoryToReturnDto), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> Add([FromBody] CreateCategoryCommand command)
        {
            var results = await _mediator.Send(command);
            return CreatedAtRoute ("GetCategory", new { id = results.Id }, results);
        }

        [HttpPut()]
        [Authorize]
        [ProducesResponseType(typeof(CategoryToReturnDto), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update([FromBody] UpdateCategoryCommand command)
        {
            var result = await _mediator.Send(command);    
            return Ok(result);
        }

        [HttpDelete("")]
        [Authorize]
        public async Task<IActionResult> Delete(DeleteCategoryCommand command)
        {
            await _mediator.Send(command);
            return NoContent();
        }

    }
}