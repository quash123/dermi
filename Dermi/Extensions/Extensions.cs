using Dermi.Domain.Exceptions;
using Dermi.Domain.Models;
// using Dermi.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Dermi.Extensions {
    public static class Extensions {
        public static string FormatExceptionToString (this DermiException dermi) {
            return JsonConvert.SerializeObject (new { dermi.StatusCode, dermi.ErrorCodeName, dermi.ClientMessage });
        }

    }
}