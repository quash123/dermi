// using System.Threading.Tasks;
// using Dermi.Data;
// using Dermi.Domain.Models;
// using Dermi.Domain.Models.Entities;
// using Microsoft.EntityFrameworkCore;
// using System.Linq;
// using System.Collections.Generic;

// namespace Dermi.Repository 
// {
//   public class DermiRepository : IDermiRepository
//   {
//       private readonly DataContext _context;

//     public DermiRepository (DataContext context) {
//       this._context = context;
//     }
//     public void Add<T> (T entity) where T : class {
//       _context.Add (entity);
//     }

//     public void Delete<T> (T entity) where T : class {
//       _context.Remove (entity);
//     }

//     public async Task<IEnumerable<Category>> GetCategories()
//     {
//       return await _context.Categories.ToListAsync();
//     }

//     public async Task<Category> GetCategory(int id)
//     {
//       return await _context.Categories.FirstOrDefaultAsync (u => u.Id == id);
//     }

//     public async Task<Category> GetCategory(string name)
//     {
//       return await _context.Categories.FirstOrDefaultAsync (u => u.Name == name);
//     }

//     public async Task<News> GetNews(int id)
//     {
//       return await _context.News.FirstOrDefaultAsync (u => u.Id == id);
//     }

//     public async Task<IEnumerable<News>> GetNews()
//     {
//       return await _context.News.ToListAsync();
//     }

//     public async Task<Product> GetProduct(int id)
//     {
//       return await _context.Products.Include(x => x.Category).FirstOrDefaultAsync (u => u.Id == id);
//     }
//     public async Task<Product> GetProduct(string name)
//     {
//       return await _context.Products.Include(x => x.Category).FirstOrDefaultAsync (u => u.Name == name);
//     }
//     public async Task<IEnumerable<Product>> GetProducts()
//     {
//       return await _context.Products.Include(x => x.Category).ToListAsync ();
//     }

//     public async Task<IEnumerable<Product>> GetProductsFromCategory(int id)
//     {
//        return await _context.Products.Include(x => x.Category).Where(p => p.Category.Id == id).ToListAsync ();
//     }

//     public async Task<User> GetUser (int id) {
//       var user = await _context.Users.FirstOrDefaultAsync (u => u.Id == id);
//       return user;
//     }
//     public async Task<bool> SaveAll () {
//       return await _context.SaveChangesAsync () > 0;
//     }

//   }
// }