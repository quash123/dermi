// using System.Collections.Generic;
// using System.Threading.Tasks;
// using Dermi.Domain.Models;
// using Dermi.Domain.Models.Entities;

// namespace Dermi.Repository 
// {
//     public interface IDermiRepository 
//     {
//         void Add<T> (T entity) where T : class;
//         void Delete<T> (T entity) where T : class;
//         Task<bool> SaveAll ();
//         Task<User> GetUser (int id);
//         Task<Product> GetProduct(int id);
//         Task<Product> GetProduct(string name);
//         Task<IEnumerable<Product>> GetProducts();
//         Task<IEnumerable<Product>> GetProductsFromCategory(int id);
//         Task<Category> GetCategory(int id);
//         Task<Category> GetCategory(string name);
//         Task<IEnumerable<Category>> GetCategories();
//         Task<News> GetNews(int id);
//         Task<IEnumerable<News>> GetNews();
//     }
// }