// using System.Threading.Tasks;
// using Dermi.Data;
// using Dermi.Domain.Models;
// using Dermi.Domain.Models.Entities;
// using Microsoft.EntityFrameworkCore;

// namespace Dermi.Repository {
//     public class AuthRepository : IAuthRepository {
//         private readonly DataContext _context;
//         public AuthRepository (DataContext context) {
//             _context = context;
//         }
//         public async Task<User> Login (string username, string password) {
//             var user = await _context.Users.FirstOrDefaultAsync (x => x.Username == username);

//             if (user == null)
//                 return null;

//             if (!VerifyPasswordHash (password, user.PasswordHash, user.PasswordSalt))
//                 return null;

//             return user;
//         }

//         public async Task<User> Register (string username, string password) {
//             byte[] passwordHash, passwordSalt;
//             CreatePasswordHash (password, out passwordHash, out passwordSalt);
//             User newUser = new User ();
//             newUser.Username = username;
//             newUser.PasswordHash = passwordHash;
//             newUser.PasswordSalt = passwordSalt;
//             await _context.Users.AddAsync (newUser);
//             await _context.SaveChangesAsync ();
//             return newUser;
//         }

//         public async Task<bool> UserExists (string username) {
//             if (await _context.Users.AnyAsync (x => x.Username == username))
//                 return true;

//             return false;
//         }
//         private void CreatePasswordHash (string password, out byte[] passwordHash, out byte[] passwordSalt) {
//             using (var hmac = new System.Security.Cryptography.HMACSHA512 ()) {
//                 passwordSalt = hmac.Key;
//                 passwordHash = hmac.ComputeHash (System.Text.Encoding.UTF8.GetBytes (password));
//             }
//         }

//         private bool VerifyPasswordHash (string password, byte[] passwordHash, byte[] passwordSalt) {
//             using (var hmac = new System.Security.Cryptography.HMACSHA512 (passwordSalt)) {
//                 var computedHash = hmac.ComputeHash (System.Text.Encoding.UTF8.GetBytes (password));
//                 for (int i = 0; i < computedHash.Length; i++) {
//                     if (computedHash[i] != passwordHash[i]) return false;
//                 }
//                 return true;
//             }
//         }
//     }
// }