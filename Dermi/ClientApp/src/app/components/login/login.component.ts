import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { AlertsService} from '../../shared/services/alerts.service';
import { AuthService } from '../../shared/services/auth.service';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../shared/ngrx/app.state';
import { LoginAction, LogoutAction } from '../../shared/ngrx/auth/auth.actions';
import { User } from '../../shared/models/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  constructor(private formBuilder: FormBuilder, 
    private alertsService: AlertsService,
    private authService: AuthService,
    private router: Router,
    private store: Store<AppState>) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      login: ['', Validators.required],
      password: ['', [Validators.required]]
    });

    this.store.dispatch(new LogoutAction());
  }

  Submit() {
    this.authService.login({username: this.loginForm.get('login').value, password: this.loginForm.get('password').value})
    .subscribe(data => {
      this.alertsService.success('Zalogowano!');
      this.router.navigate(['/admin']);
    }, error => {
      if (error.status === 401) {
        this.alertsService.error('Nieprawidłowy login lub hasło!');
      } else {
        this.alertsService.error('Wystąpił błąd serwera');
      }
    });
  }

}
