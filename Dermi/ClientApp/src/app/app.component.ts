import { AuthService } from './shared/services/auth.service';
import { Component, OnInit, Renderer2, Inject } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { AppState } from './shared/ngrx/app.state';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/platform-browser';
import { isNavbarTransparentTop } from './shared/ngrx/navbar/navbar.selectors';
import { Observable } from 'rxjs';
import { ScrollService } from './shared/services/scroll.service';
import { CategoryRequested } from './shared/ngrx/categories/categories.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private authService: AuthService, private scrollService: ScrollService) {}

  ngOnInit() {
    this.scrollService.navbarController();
    this.authService.authInit();
  }
}
