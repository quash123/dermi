import { News } from './models/news.model';
import { CategoriesService } from './services/categories.service';
import { AuthGuard } from './guard/auth.guard';
import { AuthService } from './services/auth.service';
import { NgModule, ModuleWithProviders } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from '@auth0/angular-jwt';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { NavbarEffects } from './ngrx/navbar/navbar.effects';
import { AuthEffects } from './ngrx/auth/auth.effects';
import { CategoriesEffects } from './ngrx/categories/categories.effects';
import { NewsEffects } from './ngrx/news/news.effects';
import * as fromNavbar from './ngrx/navbar/navbar.reducer';
import * as fromAuth from './ngrx/auth/auth.reducer';
import * as fromCategy from './ngrx/categories/categories.reducer';
import * as fromNews from './ngrx/news/news.reducer';
import { RouterModule } from '@angular/router';


import { ScrollService } from './services/scroll.service';
import { AlertsService } from './services/alerts.service';
import { NavbarComponent } from './components/navbar/navbar.component';

import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatPaginatorModule} from '@angular/material';
import {MatTableModule} from '@angular/material';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule, MatPaginatorIntl } from '@angular/material';

import { ErrorInterceptor } from './services/error.interceptor';
import { NewsService } from './services/news.service';


@NgModule({
  imports: [
    CommonModule,
    // BrowserAnimationsModule,
    HttpClientModule,
    MatToolbarModule,
    MatButtonModule,
    MatPaginatorModule,
    MatTableModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    RouterModule.forChild([]),
    StoreModule.forFeature('navbar', fromNavbar.navbarReducer),
    StoreModule.forFeature('auth', fromAuth.authReducer),
    StoreModule.forFeature('categories', fromCategy.categoriesReducer),
    StoreModule.forFeature('news', fromNews.newsReducer),
    EffectsModule.forFeature([NavbarEffects, AuthEffects, CategoriesEffects, NewsEffects])
  ],
  declarations: [
    NavbarComponent
  ],
  exports: [NavbarComponent, MatToolbarModule, MatButtonModule, MatNativeDateModule,
    MatPaginatorModule, MatTableModule, MatProgressSpinnerModule,
    MatInputModule, MatDialogModule, MatDatepickerModule]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [ScrollService, AlertsService, AuthService, AuthGuard, CategoriesService, NewsService,
      {
        provide: HTTP_INTERCEPTORS,
        useClass: ErrorInterceptor,
        multi: true
      },
      {
        provide: HTTP_INTERCEPTORS,
        useClass: JwtInterceptor,
        multi: true
        },
        { provide: MatPaginatorIntl, useValue: getPlPaginatorIntl() }
      ]
    };
  }
}


export function getPlPaginatorIntl() {
  const paginatorIntl = new MatPaginatorIntl();
  paginatorIntl.itemsPerPageLabel = 'Ilość rekordów na stronę:';
  paginatorIntl.getRangeLabel = plRangeLabel;
  return paginatorIntl;
}

const plRangeLabel = (page: number, pageSize: number, length: number) => {
  if (length === 0 || pageSize === 0) { return `0 z ${length}`; } 
  length = Math.max(length, 0);
  const startIndex = page * pageSize;
  const endIndex = startIndex < length ?
      Math.min(startIndex + pageSize, length) :
      startIndex + pageSize;

  return `${startIndex + 1} - ${endIndex} z ${length}`;
}