import { DermiError } from './../models/dermi-error.model';
import {Injectable} from '@angular/core';
import {HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse, HTTP_INTERCEPTORS} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            catchError(error => {
                console.log('ErrorInterceptor', error);
                if (error instanceof HttpErrorResponse) {
                    if (error.status === 401) {
                        console.log(error.status);
                        return throwError(new DermiError(error.status, 'Wymagana autoryzacja'));
                    }
                    if (error.status === 400 || error.status === 404) {
                        console.log(error.status);
                        return throwError(new DermiError(error.status, error.error));
                    }
                    return throwError('Wystąpił błąd w trakcie wywoływania żądania');
                }
            })
        );
    }
}

export const ErrorInterceptorProvider = {
    provide: HTTP_INTERCEPTORS,
    useClass: ErrorInterceptor,
    multi: true
};
