import { News } from './../models/news.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Injectable()
export class NewsService {
  constructor(private http: HttpClient) {}

  getNewsById(newsId: number): Observable<News> {
    return this.http.get<News>(
      `${environment.apiUrl}news/${newsId}`
    );
  }

  getAllNews(): Observable<News[]> {
    return this.http.get(`${environment.apiUrl}news`).pipe(
      map(res => {
        const news: News[] = res as News[];
        return news;
      })
    );
  }

  updateNews(news: News) {
    return this.http.put(`${environment.apiUrl}news`, news);
  }

  addNews(news: News) {
    return this.http.post(`${environment.apiUrl}news`, news);
  }

  deleteNews(newsId: number) {
    return this.http.delete(
      `${environment.apiUrl}news/${newsId}`,
      {}
    );
  }
}
