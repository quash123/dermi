import { LoginAction } from './../ngrx/auth/auth.actions';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from '../../../environments/environment';
import { User } from '../models/user.model';
import { Router } from '@angular/router';
import { AlertsService } from './alerts.service';
import { Store } from '@ngrx/store';
import { AppState } from '../ngrx/app.state';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  jwtHelper = new JwtHelperService();

  constructor(private http: HttpClient, 
    private alertsService: AlertsService,
    private router: Router,
    private store: Store<AppState>) {}

  login(model: any) {
    return this.http.post(environment.apiUrl + 'auth/login', model).pipe(
      map((response: any) => {
        if (response) {
          localStorage.setItem('token', response.token);     
          const user : User = response.user;
          localStorage.setItem('user', JSON.stringify(user));
          this.store.dispatch(new LoginAction(user));
        }
      })
    );
  }

  loggedIn() : boolean {
    const token = localStorage.getItem('token');
    return !this.jwtHelper.isTokenExpired(token);
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    this.alertsService.message('Wylogowano');
    this.router.navigate(['/home']);
  }

  authInit() {
    if (!this.loggedIn()) return;
    const user = localStorage.getItem('user');
    if (user) {
      const userObj : User = JSON.parse(user);
      this.store.dispatch(new LoginAction(userObj));
    }
  }
}
