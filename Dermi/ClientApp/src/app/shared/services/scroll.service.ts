import { Injectable, Inject, Renderer2, RendererFactory2 } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { DOCUMENT } from '@angular/platform-browser';
import { fromEvent } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { AppState } from '../ngrx/app.state';
import { isNavbarTransparentTop } from '../ngrx/navbar/navbar.selectors';
import {ChangeNavbarVerticalState} from '../ngrx/navbar/navbar.actions';

@Injectable({
  providedIn: 'root'
})
export class ScrollService {
  private isNavbarTransparentTop = true;
  private renderer: Renderer2;
  constructor(
    @Inject(DOCUMENT) private document: any,
    rendererFactory: RendererFactory2,
    private router: Router,
    private store: Store<AppState>
  ) {
    this.renderer = rendererFactory.createRenderer(null, null);
  }

  navbarController() {
    this.store.pipe(select(isNavbarTransparentTop)).subscribe(value => {
      this.isNavbarTransparentTop = value;
    });
    this.renderer.listen('window', 'scroll', event => {
      const number = window.scrollY;
      if (number > 150 || window.pageYOffset > 150) {
        if (this.isNavbarTransparentTop) {
          this.store.dispatch(new ChangeNavbarVerticalState());
        }
      } else {
        if (!this.isNavbarTransparentTop) {
          this.store.dispatch(new ChangeNavbarVerticalState());
        }
      }
    });
  }
}
