import { Category } from './../models/category.model';
import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Injectable()
export class CategoriesService {

    constructor(private http: HttpClient) {}

    getCategoryById(categoryId: number): Observable<Category> {
        return this.http.get<Category>(`${environment.apiUrl}categories/${categoryId}`);
    }

    getAllCategories(): Observable<Category[]> {
        return this.http.get(`${environment.apiUrl}categories`)
            .pipe(
                map(res => {
                    const categories: Category[] = res as Category[];
                    return categories;
                })
            );
    }

    updateCategory(category: Category) {
        return this.http.put(`${environment.apiUrl}categories`, category);
    }

    addCategory(category: Category) {
        return this.http.post(`${environment.apiUrl}categories`, category);
    }

    deleteCategory(categoryId: number) {
        return this.http.delete(`${environment.apiUrl}categories/${categoryId}`, {});
    }
}