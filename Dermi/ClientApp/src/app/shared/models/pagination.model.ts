export interface Pagination {
    pageNumber: number;
    itemsPerPage: number;
    totalItems: number;
    totalPages: number;
}

export interface PaginationRequest {
    pageNumber: number;
    itemsPerPage: number;
}

export class PaginatedResult<T> {
    result: T;
    pagination: Pagination;
}
