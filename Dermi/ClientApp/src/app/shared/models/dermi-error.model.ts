export class DermiError {
    public StatusCode: number;
    public ClientMessage: string;
    public ErrorCodeName: string;

    constructor(statusCode: number, clientMessage: string) {
        this.StatusCode = statusCode;
        this.ClientMessage = clientMessage;
    }
}
