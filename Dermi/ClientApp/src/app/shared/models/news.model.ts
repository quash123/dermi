export interface News {
  Id?: number;
  Added: Date;
  Title: string;
  Html: string;
}
