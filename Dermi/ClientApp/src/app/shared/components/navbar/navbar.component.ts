import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { AppState } from '../../ngrx/app.state';
import { isNavbarTransparentTop } from '../../ngrx/navbar/navbar.selectors';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  isNavbarTransparentTop = false;
  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.store
    .pipe(
      select(isNavbarTransparentTop)
    ).subscribe(isTop => {
      this.isNavbarTransparentTop = isTop;
    });
  }

}
