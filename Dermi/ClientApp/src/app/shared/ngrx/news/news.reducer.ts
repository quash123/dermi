import { News } from '../../models/news.model';
import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {NewsActions, NewsActionTypes} from './news.actions';

export interface NewsState extends EntityState<News> {
  allNewsLoaded: boolean;
}

export const adapter: EntityAdapter<News> =
  createEntityAdapter<News>();


export const initialNewsState: NewsState = adapter.getInitialState({
  allNewsLoaded: false
});


export function newsReducer(state = initialNewsState , action: NewsActions): NewsState {

  switch (action.type) {

    case NewsActionTypes.NewsLoaded:

      return adapter.addOne(action.payload.news, state);

    case NewsActionTypes.AllNewsLoaded:

      return adapter.addAll(action.payload.news, {...state, allNewsLoaded: true});

    case NewsActionTypes.NewsSaved:

      return adapter.updateOne(action.payload.news, state);

      case NewsActionTypes.NewsDeleted:

      return adapter.removeOne(action.payload.newsId, state);

    default: {

      return state;
    }

  }
}


export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal

} = adapter.getSelectors();








