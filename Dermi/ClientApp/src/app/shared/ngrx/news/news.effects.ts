import { NewsService } from './../../services/news.service';
import { AlertsService } from '../../services/alerts.service';
import { allNewsLoaded, selectAllNews, selectNewsById } from './news.selectors';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { throwError, of } from 'rxjs';
import {
  catchError,
  filter,
  map,
  mergeMap,
  withLatestFrom,
  tap
} from 'rxjs/operators';
import { AppState } from '../main.reducer';
import { select, Store } from '@ngrx/store';
import {
  NewsRequested,
  NewsActionTypes,
  NewsLoaded,
  AllNewsRequested,
  AllNewsLoaded,
  NewsDeleteRequested,
  NewsDeleted
} from './news.actions';

@Injectable()
export class NewsEffects {
  @Effect()
  loadNews$ = this.actions$.pipe(
    ofType<NewsRequested>(NewsActionTypes.NewsRequested),
    mergeMap(action => this.newsService.getNewsById(action.payload.newsId)),
    map(news => new NewsLoaded({ news })),
    catchError(err => {
      this.alertsService.error('Nie znaleziono newsa!');
      return throwError(err);
    })
  );

  @Effect()
  loadAllNews$ = this.actions$.pipe(
    ofType<AllNewsRequested>(NewsActionTypes.AllNewsRequested),
    withLatestFrom(this.store.pipe(select(allNewsLoaded))),
    // tslint:disable-next-line:no-shadowed-variable
    filter(([action, allNewsLoaded]) => !allNewsLoaded),
    mergeMap(() => this.newsService.getAllNews()),
    map(news => {
      console.log('effect map', news);
      return new AllNewsLoaded({ news })}
    ),
    catchError(err => {
      console.log('effect catch error', err);
      return throwError(err);
    })
  );

  @Effect()
  deleteNews$ = this.actions$.pipe(
    ofType<NewsDeleteRequested>(NewsActionTypes.NewsDeleteRequested),
    mergeMap(action =>
      this.newsService
        .deleteNews(action.payload.newsId)
        .pipe(map(data => action.payload.newsId))
    ),
    map(newsId => new NewsDeleted({ newsId })),
    catchError(err => {
      this.alertsService.error('Wystąpił błąd w trakcie usuwania newsa!');
      return throwError(err);
    })
  );

  constructor(
    private actions$: Actions,
    private newsService: NewsService,
    private alertsService: AlertsService,
    private store: Store<AppState>
  ) {}
}
