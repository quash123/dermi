import { Action } from '@ngrx/store';
import { News } from '../../models/news.model';
import { Update } from '@ngrx/entity';

export enum NewsActionTypes {
  NewsRequested = '[News] News Requested',
  NewsLoaded = '[News] News Loaded',
  AllNewsRequested = '[News] All News Requested',
  AllNewsLoaded  = '[News] All News Loaded',
  NewsSaved = '[News] News Saved',
  NewsNoTFound = '[News] News Not Found',
  NewsDeleteRequested = '[News] News Delete Requested',
  NewsDeleted = '[News] News Deleted'
}

export class NewsRequested implements Action {

  readonly type = NewsActionTypes.NewsRequested;

  constructor(public payload: { newsId: number }) {

  }
}
export class NewsLoaded implements Action {

  readonly type = NewsActionTypes.NewsLoaded;

  constructor(public payload: { news: News }) {
  }
}

export class NewsDeleteRequested implements Action {

  readonly type = NewsActionTypes.NewsDeleteRequested;

  constructor(public payload: { newsId: number }) {

  }
}
export class NewsDeleted implements Action {

  readonly type = NewsActionTypes.NewsDeleted;

  constructor(public payload: { newsId: number }) {
  }
}


export class AllNewsRequested implements Action {

  readonly type = NewsActionTypes.AllNewsRequested;

}

export class AllNewsLoaded implements Action {

  readonly type = NewsActionTypes.AllNewsLoaded;

  constructor(public payload: { news: News[] }) {

  }

}

export class NewsSaved implements Action {

  readonly type = NewsActionTypes.NewsSaved;

  constructor(public payload: { news: Update<News> }) {}
}
export type NewsActions = NewsRequested | NewsLoaded | AllNewsRequested
| AllNewsLoaded | NewsSaved | NewsDeleteRequested | NewsDeleted;
