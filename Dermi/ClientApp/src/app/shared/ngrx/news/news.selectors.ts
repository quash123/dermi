import { AllNewsRequested } from './news.actions';
import {createFeatureSelector, createSelector} from '@ngrx/store';
import {NewsState} from './news.reducer';

import * as fromNews from './news.reducer';

export const selectNewsState = createFeatureSelector<NewsState>('news');

export const selectNewsById = (newsId: number) => createSelector(
  selectNewsState,
  state => state.entities[newsId]
);

export const selectAllNews = createSelector(
  selectNewsState,
  fromNews.selectAll

);

export const allNewsLoaded = createSelector(
  selectNewsState,
  state => state.allNewsLoaded
);
