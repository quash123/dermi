import { Action } from '@ngrx/store';
import { AuthActions, AuthActionTypes, LoginAction} from './auth.actions';
import { User } from '../../models/user.model';

export interface AuthState {
  loggedIn: boolean;
  user: User;
}

export const initialState: AuthState = {
  loggedIn: false,
  user: undefined
};

export function authReducer(state = initialState, action: Action): AuthState {  
  switch (action.type) {
    case AuthActionTypes.LoginAction:
      return {
        loggedIn: true,
        user: (action as LoginAction).user
      };
    case AuthActionTypes.LogoutAction:
      return {
        loggedIn: false,
        user: undefined
      };
    default:
      return state;
  }
}
