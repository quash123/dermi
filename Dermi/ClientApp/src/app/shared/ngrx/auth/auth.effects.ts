import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { AuthActionTypes, LoginAction, LogoutAction } from './auth.actions';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';


@Injectable()
export class AuthEffects {

  constructor(private actions$: Actions, private router: Router) {}

  @Effect({dispatch:false})
  login$ = this.actions$.pipe(
    ofType<LoginAction>(AuthActionTypes.LoginAction),
    tap(action => localStorage.setItem("user", JSON.stringify(action.user)))
  );

  @Effect({dispatch:false})
  logout$ = this.actions$.pipe(
    ofType<LogoutAction>(AuthActionTypes.LogoutAction),
    tap(() => {
      localStorage.removeItem("user");
      localStorage.removeItem("token");
      this.router.navigateByUrl('/login');
    })
  );
}
