import { Action } from '@ngrx/store';

export enum NavbarActionTypes {
  ChangeNavbarVerticalState = '[Navbar] Navbar Vertical State Changed'
}

export class ChangeNavbarVerticalState implements Action {
  readonly type = NavbarActionTypes.ChangeNavbarVerticalState;
}

export type NavbarActions = ChangeNavbarVerticalState;
