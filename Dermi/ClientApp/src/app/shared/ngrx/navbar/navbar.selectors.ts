import {createSelector} from '@ngrx/store';


export const selectNavbarState = state => state.navbar;


export const isNavbarTransparentTop = createSelector(
  selectNavbarState,
  navbar => navbar.isNavbarTransparentTop
);

