import { Action } from '@ngrx/store';
import { NavbarActions, NavbarActionTypes } from './navbar.actions';

export interface NavbarState {
  isNavbarTransparentTop: boolean;
}

export const initialState: NavbarState = {
  isNavbarTransparentTop: true
};

export function navbarReducer(state = initialState, action: Action): NavbarState {
  switch (action.type) {
    case NavbarActionTypes.ChangeNavbarVerticalState:
      return {
        isNavbarTransparentTop: !state.isNavbarTransparentTop
      };
    default:
      return state;
  }
}
