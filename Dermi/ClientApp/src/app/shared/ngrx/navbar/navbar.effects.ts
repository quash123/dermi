import { tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { NavbarActionTypes, ChangeNavbarVerticalState} from './navbar.actions';

@Injectable()
export class NavbarEffects {

  @Effect({dispatch: false})
  verticalNavbarState$ = this.actions$.pipe(
    ofType<ChangeNavbarVerticalState>(NavbarActionTypes.ChangeNavbarVerticalState),
    tap((data) => {
    //console.log('tap data', data);
  }));

  constructor(private actions$: Actions) {}
}
