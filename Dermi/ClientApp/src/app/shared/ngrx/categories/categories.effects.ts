import { AlertsService } from '../../services/alerts.service';
import { allCategoriesLoaded, selectAllCategories, selectCategoryById } from './categories.selectors';
import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import { CategoryActions, CategoryActionTypes, CategoryRequested, CategoryLoaded,
  AllCategoriesRequested, AllCategoriesLoaded, CategoryDeleteRequested, CategoryDeleted} from './categories.actions';
import {throwError, of} from 'rxjs';
import {catchError, filter, map, mergeMap, withLatestFrom, tap} from 'rxjs/operators';
import { CategoriesService } from '../../services/categories.service';
import {AppState} from '../main.reducer';
import {select, Store} from '@ngrx/store';

@Injectable()
export class CategoriesEffects {

  @Effect()
  loadCategory$ = this.actions$
    .pipe(
      ofType<CategoryRequested>(CategoryActionTypes.CategoryRequested),
      mergeMap(action => this.categoriesService.getCategoryById(action.payload.categoryId)),
      map(category => new CategoryLoaded({ category })),
      catchError(err => {
        this.alertsService.error('Nie znaleziono kategorii!');
        return throwError(err);
      })
  );

  @Effect()
  loadAllCategories$ = this.actions$
    .pipe(
      ofType<AllCategoriesRequested>(CategoryActionTypes.AllCategoriesRequested),
      withLatestFrom(this.store.pipe(select(allCategoriesLoaded))),
      // tslint:disable-next-line:no-shadowed-variable
      filter(([action, allCategoriesLoaded]) => !allCategoriesLoaded),
      mergeMap(() => this.categoriesService.getAllCategories()),
      map(categories => new AllCategoriesLoaded({categories})),
      catchError(err => {
        return throwError(err);
      })
    );

    @Effect()
    deleteCategory$ = this.actions$
    .pipe(
      ofType<CategoryDeleteRequested>(CategoryActionTypes.CategoryDeleteRequested),
      mergeMap(action => this.categoriesService.deleteCategory(action.payload.categoryId).pipe(
        map(data => action.payload.categoryId)
      )),
      map(categoryId => new CategoryDeleted({ categoryId })),
      catchError(err => {
        this.alertsService.error('Wystąpił błąd w trakcie usuwania kategorii!');
        return throwError(err);
      })
  );

  constructor(private actions$: Actions,
    private categoriesService: CategoriesService,
    private alertsService: AlertsService,
    private store: Store<AppState>) {}
}
