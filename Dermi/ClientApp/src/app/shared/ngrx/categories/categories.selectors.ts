import {createFeatureSelector, createSelector} from '@ngrx/store';
import {CategoriesState} from './categories.reducer';

import * as fromCategories from './categories.reducer';

export const selectCategoriesState = createFeatureSelector<CategoriesState>('categories');

export const selectCategoryById = (categoryId: number) => createSelector(
    selectCategoriesState,
  categoriesState => categoriesState.entities[categoryId]
);

export const selectAllCategories = createSelector(
    selectCategoriesState,
    fromCategories.selectAll

);

export const allCategoriesLoaded = createSelector(
    selectCategoriesState,
    categoriesState => categoriesState.allCategoriesLoaded
);

export const selectCategoriesPage = (pageIndex: number, pageSize: number) => createSelector(
    selectAllCategories,
    allCategories => {
      const start = pageIndex * pageSize;
      const end = start + pageSize;
      return allCategories.slice(start, end);
    }
  );


