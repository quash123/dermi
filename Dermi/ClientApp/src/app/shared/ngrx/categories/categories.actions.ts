import { Action } from '@ngrx/store';
import { Category } from '../../models/category.model';
import { Update } from '@ngrx/entity';

export enum CategoryActionTypes {
  CategoryRequested = '[Category] Category Requested',
  CategoryLoaded = '[Category] Category Loaded',
  AllCategoriesRequested = '[Category] All Categories Requested',
  AllCategoriesLoaded  = '[Category] All Categories Loaded',
  CategorySaved = '[Category] Category Saved',
  CategoryNoTFound = '[Category] Category Not Found',
  CategoryDeleteRequested = '[Category] Category Delete Requested',
  CategoryDeleted = '[Category] Category Deleted'
}

export class CategoryRequested implements Action {

  readonly type = CategoryActionTypes.CategoryRequested;

  constructor(public payload: { categoryId: number }) {

  }
}
export class CategoryLoaded implements Action {

  readonly type = CategoryActionTypes.CategoryLoaded;

  constructor(public payload: { category: Category }) {
  }
}

export class CategoryDeleteRequested implements Action {

  readonly type = CategoryActionTypes.CategoryDeleteRequested;

  constructor(public payload: { categoryId: number }) {

  }
}
export class CategoryDeleted implements Action {

  readonly type = CategoryActionTypes.CategoryDeleted;

  constructor(public payload: { categoryId: number }) {
  }
}



export class CategoryNoTFound implements Action {

  readonly type = CategoryActionTypes.CategoryRequested;

  constructor() {
  }
}

export class AllCategoriesRequested implements Action {

  readonly type = CategoryActionTypes.AllCategoriesRequested;

}

export class AllCategoriesLoaded implements Action {

  readonly type = CategoryActionTypes.AllCategoriesLoaded;

  constructor(public payload: { categories: Category[] }) {

  }

}

export class CategorySaved implements Action {

  readonly type = CategoryActionTypes.CategorySaved;

  constructor(public payload: { category: Update<Category> }) {}
}
export type CategoryActions = CategoryRequested | CategoryLoaded | AllCategoriesRequested
| AllCategoriesLoaded | CategorySaved | CategoryNoTFound | CategoryDeleteRequested | CategoryDeleted;
