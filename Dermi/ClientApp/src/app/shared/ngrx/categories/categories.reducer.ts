import { Category } from '../../models/category.model';
import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {CategoryActions, CategoryActionTypes} from './categories.actions';

export interface CategoriesState extends EntityState<Category> {
  allCategoriesLoaded: boolean;
}

export const adapter: EntityAdapter<Category> =
  createEntityAdapter<Category>();


export const initialCategoriesState: CategoriesState = adapter.getInitialState({
  allCategoriesLoaded: false
});


export function categoriesReducer(state = initialCategoriesState , action: CategoryActions): CategoriesState {

  switch (action.type) {

    case CategoryActionTypes.CategoryLoaded:

      return adapter.addOne(action.payload.category, state);

    case CategoryActionTypes.AllCategoriesLoaded:

      return adapter.addAll(action.payload.categories, {...state, allCategoriesLoaded: true});

    case CategoryActionTypes.CategorySaved:

      return adapter.updateOne(action.payload.category, state);

      case CategoryActionTypes.CategoryDeleted:

      return adapter.removeOne(action.payload.categoryId, state);

    default: {

      return state;
    }

  }
}


export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal

} = adapter.getSelectors();








