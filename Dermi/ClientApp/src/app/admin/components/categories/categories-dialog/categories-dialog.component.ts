import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CategorySaved } from './../../../../shared/ngrx/categories/categories.actions';
import { DermiError } from './../../../../shared/models/dermi-error.model';
import { AlertsService } from './../../../../shared/services/alerts.service';
import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../shared/ngrx/app.state';
import { CategoriesService } from '../../../../shared/services/categories.service';
import { Category } from '../../../../shared/models/category.model';
import { CategoryLoaded } from '../../../../shared/ngrx/categories/categories.actions';
import { Update } from '@ngrx/entity';
import { Router } from '@angular/router';
import { of } from 'rxjs';

@Component({
  selector: 'app-categories-dialog',
  templateUrl: './categories-dialog.component.html',
  styleUrls: ['./categories-dialog.component.css']
})
export class CategoriesDialogComponent implements OnInit {
  categoryId: number;
  categoryModal: CategoryModal;
  form: FormGroup;
  name: string;
  title: string;
  isNew: boolean;
  constructor(
    private store: Store<AppState>,
    private categoriesService: CategoriesService,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<CategoriesDialogComponent>,
    private alertsService: AlertsService,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) category: CategoryModal
  ) {
    this.categoryModal = category;
  }

  ngOnInit() {
    this.isNew = this.categoryModal.isNew;
    this.title = this.isNew ? 'Dodaj Kategorię' : 'Edytuj Kategorię';
    this.categoryId = this.categoryModal.Id;
    this.name = this.categoryModal.Name;
    this.form = this.fb.group({
      name: [this.categoryModal.Name, Validators.required]
    });
  }

  save() {
    const changes = this.form.value;
    if (this.isNew) {
      const category = { Name: this.form.get('name').value } as Category;
      this.categoriesService.addCategory(category)
      .pipe(catchError(error => throwError(error)))
      .subscribe(
        data => {
          this.alertsService.success('Kategoria dodana!');
          this.store.dispatch(
            new CategoryLoaded({ category: data as Category })
          );
          this.dialogRef.close();
        },
        error => {
          if (error instanceof DermiError) {
            const dermiError = error;
            this.alertsService.error(error.ClientMessage);
            if (dermiError.StatusCode === 401) {
              this.dialogRef.close();
              this.router.navigate(['/login']);
            }
          } else {
            this.alertsService.error(
              'Wystąpił błąd w trakcie dodawania kategorii'
            );
          }
        }
      );
    } else {
      if ( this.form.get('name').value === this.categoryModal.Name) {
        this.alertsService.warning('Brak zmian');
        return;
      }
      const category = { Id: this.categoryModal.Id, Name: this.form.get('name').value } as Category;
      this.categoriesService.updateCategory(category).pipe(
        catchError(error => throwError(error))
      ).subscribe(
        data => {
          const categoryUpdate: Update<Category> = {
            id: this.categoryModal.Id,
            changes
          };
          this.store.dispatch(
            new CategorySaved({ category: categoryUpdate })
          );
          this.alertsService.success('Kategoria zaktualizowana!');
          this.dialogRef.close();
        },
        error => {
          if (error instanceof DermiError) {
            const dermiError = error;
            this.alertsService.error(error.ClientMessage);
            if (dermiError.StatusCode === 401) {
              this.dialogRef.close();
              this.router.navigate(['/login']);
            }
          } else {
            this.alertsService.error(
              'Wystąpił błąd w trakcie aktualizacji kategorii'
            );
          }
        }
      );
    }
  }

  close() {
    this.dialogRef.close();
  }
}

export interface CategoryModal extends Category {
  isNew: boolean;
}
