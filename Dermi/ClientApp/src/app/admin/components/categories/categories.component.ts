import { AlertsService } from './../../../shared/services/alerts.service';
import {AllCategoriesRequested, CategoryDeleteRequested} from './../../../shared/ngrx/categories/categories.actions';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { AppState } from '../../../shared/ngrx/app.state';
import { Category } from '../../../shared/models/Category.model';
import {selectAllCategories, selectCategoriesPage} from '../../../shared/ngrx/categories/categories.selectors';
import {tap} from 'rxjs/operators';
import {MatPaginator, MatDialogConfig} from '@angular/material';
import { MatDialog} from '@angular/material';
import {CategoriesDialogComponent, CategoryModal} from './categories-dialog/categories-dialog.component';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  categories: Category[] = [];
  category: Category;
  displayedColumns: string[] = ['index', 'name', 'action'];
  isLoadingResults = true;
  isRateLimitReached = false;
  recordsCount = 0;
  pageSize = 5;
  pageIndex = 0;
  // pageEvent: PageEvent;


  @ViewChild(MatPaginator)
  paginator: MatPaginator;

  constructor(
    private store: Store<AppState>,
    private alertsService: AlertsService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.store
      .pipe(
        select(selectAllCategories),
        tap(categories => {
          if (categories.length === 0) {
            this.store.dispatch(new AllCategoriesRequested());
          }
        })
      )
      .subscribe(
        data => {
          this.isLoadingResults = false;
          this.recordsCount = data.length;
          this.getPagedData();
        },
        error => {
          this.alertsService.error('Wystąpił błąd w trakcie ładowania danych!');
        }
      );
    
  }
  
  onPageChange(e : any) {
    this.pageIndex = e.pageIndex;
    this.pageSize = e.pageSize;
    this.getPagedData();
  }

  getPagedData() {
    this.isLoadingResults = true;
    this.store
    .pipe(
      select(selectCategoriesPage(this.pageIndex, this.pageSize)))
    .subscribe(
      data => {
        this.categories = data;
        this.isLoadingResults = false;
      },
      error => {
        this.alertsService.error('Wystąpił błąd w trakcie ładowania danych!');
      }
    );
  }

  edit(data) {
    const category = {
      isNew: false,
      Id: data.id,
      Name: data.name
    } as CategoryModal;
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '400px';
    dialogConfig.data = category;
    const dialogRef = this.dialog.open(CategoriesDialogComponent, dialogConfig);
  }

  deleted(category) {
    this.alertsService.confirm(
      'Potwierdzenie',
      `Na pewno usunąć kategorię: '${category.name}'`,
      () => {
        this.store.dispatch(
          new CategoryDeleteRequested({ categoryId: category.id })
        );
      }
    );
  }

  add() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '400px';
    dialogConfig.data = { isNew: true } as CategoryModal;

    this.dialog.open(CategoriesDialogComponent, dialogConfig);
  }
}
