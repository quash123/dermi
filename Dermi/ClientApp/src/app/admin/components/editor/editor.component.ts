import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Observable } from 'rxjs';
import * as QuillNamespace from 'quill';
import { QuillEditorComponent } from 'ngx-quill/src/quill-editor.component';
let Quill: any = QuillNamespace;
import ImageResize from 'quill-image-resize-module';
Quill.register('modules/imageResize', ImageResize);


@Component({
  selector: 'admin-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent implements OnInit {
  @Input()
  InitialData: string;
  @Output()
  change: EventEmitter<string> = new EventEmitter<string>();

  editor_modules: any;
  editorData = '';
  @ViewChild('editor') editor: QuillEditorComponent;
  constructor() { }

  ngOnInit() {
    this.editorData = this.InitialData;
    console.log('this.InitialData', this.InitialData);
    this.editor_modules = {
      toolbar: {
        container: [
          ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
          ['blockquote', 'code-block'],
          [{ 'header': 1 }, { 'header': 2 }],               // custom button values
          [{ 'list': 'ordered'}, { 'list': 'bullet' }],
          [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
          [{ 'direction': 'rtl' }],                         // text direction
          [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
          [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
          [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
          [{ 'align': [] }],
          ['clean'],                                         // remove formatting button
          ['link', 'image']                         // link and image, video
        ]
      },
      imageResize: true
    };

    this.editor
      .onContentChanged
      .pipe(
        debounceTime(400),
        distinctUntilChanged()
      )
      .subscribe(data => {
        this.change.emit(this.editorData);
      });
  }

}
