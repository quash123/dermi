import { NewsLoaded, NewsSaved } from './../../../../shared/ngrx/news/news.actions';
import { NewsService } from './../../../../shared/services/news.service';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { DermiError } from './../../../../shared/models/dermi-error.model';
import { AlertsService } from './../../../../shared/services/alerts.service';
import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../shared/ngrx/app.state';
import { Update } from '@ngrx/entity';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { News } from '../../../../shared/models/news.model';

@Component({
  selector: 'app-news-dialog',
  templateUrl: './news-dialog.component.html',
  styleUrls: ['./news-dialog.component.scss']
})
export class NewsDialogComponent implements OnInit {
  newsId: number;
  newsModal: NewsModal;
  form: FormGroup;
  title = '';
  isNew: boolean;
  html = '';
  constructor(
    private store: Store<AppState>,
    private newsService: NewsService,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<NewsDialogComponent>,
    private alertsService: AlertsService,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) news: NewsModal
  ) {
    this.newsModal = news;
  }

  ngOnInit() {
    this.isNew = this.newsModal.isNew;
    this.title = this.isNew ? 'Dodaj Newsa' : 'Edytuj Newsa';
    this.newsId = this.newsModal.Id;
    this.form = this.fb.group({
      title: [this.newsModal.Title, Validators.required],
      added: [this.newsModal.Added, Validators.required],
      html: [this.newsModal.Html, Validators.required]
    });

    this.html = this.newsModal.Html;
  }

  save() {
    const changes = this.form.value;
    console.log('changes', changes);
    console.log('is new', this.isNew);
    if (this.isNew) {
      const news = { Title: this.form.get('title').value,
      Added: this.form.get('added').value, Html: this.form.get('html').value } as News;
      this.newsService.addNews(news)
      .pipe(catchError(error => throwError(error)))
      .subscribe(
        data => {
          this.alertsService.success('News dodany!');
          this.store.dispatch(
            new NewsLoaded({ news: data as News })
          );
          this.dialogRef.close();
        },
        error => {
          if (error instanceof DermiError) {
            const dermiError = error;
            this.alertsService.error(error.ClientMessage);
            if (dermiError.StatusCode === 401) {
              this.dialogRef.close();
              this.router.navigate(['/login']);
            }
          } else {
            this.alertsService.error(
              'Wystąpił błąd w trakcie dodawania newsa!'
            );
          }
        }
      );
    } else {
      if ( this.form.get('title').value === this.newsModal.Title &&
          this.form.get('added').value === this.newsModal.Added &&
          this.form.get('html').value === this.newsModal.Html) {
        this.alertsService.warning('Brak zmian');
        return;
      }
      const news = { Id: this.newsModal.Id, Title: this.form.get('title').value,
      Added: this.form.get('added').value, Html: this.form.get('html').value } as News;
      this.newsService.updateNews(news).pipe(
        catchError(error => throwError(error))
      ).subscribe(
        data => {
          const newsUpdate: Update<News> = {
            id: this.newsModal.Id,
            changes
          };
          this.store.dispatch(
            new NewsSaved({ news: newsUpdate })
          );
          this.alertsService.success('News zaktualizowany!');
          this.dialogRef.close();
        },
        error => {
          if (error instanceof DermiError) {
            const dermiError = error;
            this.alertsService.error(error.ClientMessage);
            if (dermiError.StatusCode === 401) {
              this.dialogRef.close();
              this.router.navigate(['/login']);
            }
          } else {
            this.alertsService.error(
              'Wystąpił błąd w trakcie aktualizacji newsa!'
            );
          }
        }
      );
    }
  }

  editorChanged(e) {
    this.html = e;
    this.form.controls['html'].setValue(this.html);
  }

  close() {
    this.dialogRef.close();
  }
}

export interface NewsModal extends News {
  isNew: boolean;
}
