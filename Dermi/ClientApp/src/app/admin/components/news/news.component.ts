import { AllNewsRequested, NewsDeleteRequested } from './../../../shared/ngrx/news/news.actions';
import { News } from './../../../shared/models/news.model';
import { AlertsService } from './../../../shared/services/alerts.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { AppState } from '../../../shared/ngrx/app.state';
import {selectAllNews} from '../../../shared/ngrx/news/news.selectors';
import { tap, catchError } from 'rxjs/operators';
import {MatPaginator, MatDialogConfig} from '@angular/material';
import { MatDialog} from '@angular/material';
import {NewsDialogComponent, NewsModal} from './news-dialog/news-dialog.component';
import { of } from 'rxjs';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'admin-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
  news: News[] = [];
  editNews: News;
  displayedColumns: string[] = ['index', 'name', 'action'];
  textFormat = 'Hello World!';
  isLoadingResults = true;
  isRateLimitReached = false;
  @ViewChild(MatPaginator)
  paginator: MatPaginator;

  constructor(
    private store: Store<AppState>,
    private alertsService: AlertsService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.store
      .pipe(
        select(selectAllNews),
        tap(news => {
          console.log('tap news', news);
          this.store.dispatch(new AllNewsRequested());
        }),
        catchError(
          error => {
            console.log('catchError', error);
            return of([]);
          }
        )
      )
      .subscribe(
        data => {
          console.log('component data', data);
          this.isLoadingResults = false;
          this.news = data;
        },
        error => {
          console.log('errpr', error)
          this.alertsService.error('Wystąpił błąd w trakcie ładowania danych!');
        }
      );
  }
  editorChanged(e) {
    console.log('editorChanged', e);
  }

  deleted(news) {
    this.alertsService.confirm(
      'Potwierdzenie',
      `Na pewno usunąć news: '${news.title}'`,
      () => {
        this.store.dispatch(
          new NewsDeleteRequested({ newsId: news.id })
        );
      }
    );
  }

  edit(data) {
    console.log('editd', data);
    const news = {
      isNew: false,
      Id: data.id,
      Title: data.title,
      Added: data.added,
      Html: data.html,
    } as NewsModal;
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '600px';
    dialogConfig.data = news;
    const dialogRef = this.dialog.open(NewsDialogComponent, dialogConfig);
  }


   
  add() {
    const news = {
      isNew: true,
      Id: null,
      Title: '',
      Html: ''
    } as NewsModal;
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '600px';
    dialogConfig.data = news;

    this.dialog.open(NewsDialogComponent, dialogConfig);
  }
}
