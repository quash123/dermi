import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin.routing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { QuillModule } from 'ngx-quill';


import { CategoriesComponent } from './components/categories/categories.component';
import { EditorComponent } from './components/editor/editor.component';
import { CategoriesDialogComponent } from './components/categories/categories-dialog/categories-dialog.component';
import { SharedModule } from '../shared/shared.module';
import { NewsComponent } from './components/news/news.component';
import { NewsDialogComponent } from './components/news/news-dialog/news-dialog.component';
@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    QuillModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule.forRoot(),
  ],
  entryComponents: [CategoriesDialogComponent, NewsDialogComponent],
  declarations: [CategoriesComponent, EditorComponent, CategoriesDialogComponent, NewsComponent, NewsDialogComponent]
})
export class AdminModule { }
