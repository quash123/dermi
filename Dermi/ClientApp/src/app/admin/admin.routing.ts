import { NewsComponent } from './components/news/news.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: 'kategorie',
    component: CategoriesComponent
  },
  {
    path: 'newsy',
    component: NewsComponent
  },
  {
    path: '**',
    redirectTo: '/admin/kategorie'
  }
];

@NgModule({
  imports: [
      RouterModule.forChild(routes)
  ],
  exports: [
      RouterModule
  ],
})
export class AdminRoutingModule { }

//export const AdminRoutes = RouterModule.forChild(routes);
