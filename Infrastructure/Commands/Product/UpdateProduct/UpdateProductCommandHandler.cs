using MediatR;
using Dermi.Domain.Dtos;
using System.Threading.Tasks;
using System.Threading;
using Dermi.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using Dermi.Domain.Models.Entities;
using AutoMapper;
using Dermi.Domain.Exceptions;
using Infrastructure.Extensions;

namespace Infrastructure.Commands
{
    public class UpdateProductCommandHandler : IRequestHandler<UpdateProductCommand, ProductToReturnDto>
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public UpdateProductCommandHandler(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ProductToReturnDto> Handle(UpdateProductCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Products
                .FirstOrDefaultAsync(c => c.Id == request.Id, cancellationToken);

            if (entity == null)
                throw new DermiException("Wybrany produkt nie istnieje", "dermi_404", 404);

            entity = _mapper.Map<Dermi.Domain.Models.Entities.Product>(request);
            await _context.SaveDb();
            return _mapper.Map<ProductToReturnDto>(entity);
        }
    }
}