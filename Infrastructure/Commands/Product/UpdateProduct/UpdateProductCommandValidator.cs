using FluentValidation;

namespace Infrastructure.Commands.Product.UpdateProduct
{
    public class UpdateProductCommandValidator : AbstractValidator<UpdateProductCommand>
    {
        public UpdateProductCommandValidator()
        {
            RuleFor(v => v.Id).GreaterThan(0);
            RuleFor(v => v.Name).NotEmpty();
            RuleFor(v => v.CategoryId).GreaterThan(0);
        }
    }
}