using Dermi.Domain.Dtos;
using MediatR;

namespace Infrastructure.Commands
{
    public class UpdateProductCommand : IRequest<ProductToReturnDto>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal? Price {get; set;}
        public int CategoryId {get; set;}
    }
}