using MediatR;
using Dermi.Domain.Dtos;
using System.Threading.Tasks;
using System.Threading;
using Dermi.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using Dermi.Domain.Models.Entities;
using AutoMapper;
using Dermi.Domain.Exceptions;
using System.Linq;

namespace Infrastructure.Commands
{
    public class DeleteProductCommandHandler : IRequestHandler<DeleteProductCommand, Unit>
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public DeleteProductCommandHandler(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(DeleteProductCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Products
                .FirstOrDefaultAsync(c => c.Id == request.Id, cancellationToken);

            if (entity == null)
                throw new DermiException("Wybrany produkt nie istnieje", "dermi_404", 404);
            
            _context.Products.Remove(entity);

            if (await _context.SaveChangesAsync(cancellationToken) <= 0)
                throw new DermiException("Wystąpił błąd w trakcie zapisu do bazy danych!", "dermi_500", 500);

            return Unit.Value;
        }
    }
}