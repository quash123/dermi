using FluentValidation;

namespace Infrastructure.Commands.Product.DeleteProduct
{
    public class DeleteProductCommandValidator : AbstractValidator<DeleteProductCommand>
    {
        public DeleteProductCommandValidator()
        {
            RuleFor(v => v.Id).GreaterThan(0);
        }
    }
}