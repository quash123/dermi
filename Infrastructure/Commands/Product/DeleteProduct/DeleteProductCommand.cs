using Dermi.Domain.Dtos;
using MediatR;

namespace Infrastructure.Commands
{
    public class DeleteProductCommand : IRequest
    {
        public int Id { get; set; }
    }
}