using FluentValidation;

namespace Infrastructure.Commands.Product.CreateProduct
{
    public class CreateProductCommandValidator : AbstractValidator<CreateProductCommand>
    {
        public CreateProductCommandValidator()
        {
            RuleFor(v => v.Name).NotEmpty();
        }
    }
}