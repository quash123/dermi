using Dermi.Domain.Dtos;
using MediatR;

namespace Infrastructure.Commands
{
    public class CreateProductCommand : IRequest<ProductToReturnDto>
    {
        public string Name { get; set; }
    }
}