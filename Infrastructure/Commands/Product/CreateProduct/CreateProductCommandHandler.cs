using MediatR;
using Dermi.Domain.Dtos;
using System.Threading.Tasks;
using System.Threading;
using Dermi.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using Dermi.Domain.Models.Entities;
using AutoMapper;
using Dermi.Domain.Exceptions;
using Infrastructure.Extensions;

namespace Infrastructure.Commands
{
    public class CreateProductCommandHandler : IRequestHandler<CreateProductCommand, ProductToReturnDto>
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public CreateProductCommandHandler(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ProductToReturnDto> Handle(CreateProductCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Products
                .FirstOrDefaultAsync(c => c.Name == request.Name, cancellationToken);

            if (entity != null)
                throw new DermiException("Produkt o podanej nazwie już istnieje", "dermi_400", 400);

            Dermi.Domain.Models.Entities.Product product = new Dermi.Domain.Models.Entities.Product { Name = request.Name};
            _context.Products.Add(product);

            await _context.SaveDb();

            return _mapper.Map<ProductToReturnDto>(product);
        }
    }
}