using Dermi.Domain.Dtos;
using MediatR;

namespace Infrastructure.Commands
{
    public class CreateNewsCommand : IRequest<NewsToReturnDto>
    {
        public string Title {get; set;}
        public string Html {get; set;}
        public string PhotoUrl {get; set;}
    }
}