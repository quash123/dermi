using FluentValidation;

namespace Infrastructure.Commands.News.CreateNews
{
    public class CreateNewsCommandValidator : AbstractValidator<CreateNewsCommand>
    {
        public CreateNewsCommandValidator()
        {
            RuleFor(v => v.Title).NotEmpty();           
        }
    }
}