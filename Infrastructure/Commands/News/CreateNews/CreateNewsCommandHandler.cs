using MediatR;
using Dermi.Domain.Dtos;
using System.Threading.Tasks;
using System.Threading;
using Dermi.Infrastructure.Data;
using System;
using AutoMapper;
using Dermi.Domain.Exceptions;
using Dermi.Domain.Models.Entities;
using Infrastructure.Extensions;

namespace Infrastructure.Commands
{
    public class CreateNewsCommandHandler : IRequestHandler<CreateNewsCommand, NewsToReturnDto>
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public CreateNewsCommandHandler(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<NewsToReturnDto> Handle(CreateNewsCommand request, CancellationToken cancellationToken)
        {
            Dermi.Domain.Models.Entities.News News = new Dermi.Domain.Models.Entities.News { Title = request.Title, Added = DateTime.Now, PhotoUrl = request.PhotoUrl, Html = request.Html };
            _context.News.Add(News);
            await _context.SaveDb();

            return _mapper.Map<NewsToReturnDto>(News);
        }
    }
}