using FluentValidation;

namespace Infrastructure.Commands.News.UpdateCategory
{
    public class UpdateNewsCommandValidator : AbstractValidator<UpdateNewsCommand>
    {
        public UpdateNewsCommandValidator()
        {
            RuleFor(v => v.Id).GreaterThan(0);
            RuleFor(v => v.Title).NotEmpty();
        }
    }
}