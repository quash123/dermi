using Dermi.Domain.Dtos;
using MediatR;

namespace Infrastructure.Commands
{
    public class UpdateNewsCommand : IRequest<NewsToReturnDto>
    {
        public int Id { get; set; }    
        public string Title {get; set;}
        public string Html {get; set;}
        public string PhotoUrl {get; set;}
    }
}