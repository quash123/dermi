using MediatR;
using Dermi.Domain.Dtos;
using System.Threading.Tasks;
using System.Threading;
using Dermi.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using Dermi.Domain.Models.Entities;
using AutoMapper;
using Dermi.Domain.Exceptions;
using Infrastructure.Extensions;

namespace Infrastructure.Commands
{
    public class UpdateNewsCommandHandler : IRequestHandler<UpdateNewsCommand, NewsToReturnDto>
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public UpdateNewsCommandHandler(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<NewsToReturnDto> Handle(UpdateNewsCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.News
                .FirstOrDefaultAsync(c => c.Id == request.Id, cancellationToken);

            if (entity == null)
                throw new DermiException("Wybrany news nie istnieje", "dermi_404", 404);

            entity = _mapper.Map<Dermi.Domain.Models.Entities.News>(request);
            await _context.SaveDb();
            
            return _mapper.Map<NewsToReturnDto>(entity);
        }
    }
}