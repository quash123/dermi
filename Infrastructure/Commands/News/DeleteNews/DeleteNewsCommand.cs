using Dermi.Domain.Dtos;
using MediatR;

namespace Infrastructure.Commands
{
    public class DeleteNewsCommand : IRequest
    {
        public int Id { get; set; }
    }
}