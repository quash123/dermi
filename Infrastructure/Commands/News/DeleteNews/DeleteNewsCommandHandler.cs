using MediatR;
using Dermi.Domain.Dtos;
using System.Threading.Tasks;
using System.Threading;
using Dermi.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using Dermi.Domain.Models.Entities;
using AutoMapper;
using Dermi.Domain.Exceptions;
using System.Linq;
using Infrastructure.Extensions;

namespace Infrastructure.Commands
{
    public class DeleteNewsCommandHandler : IRequestHandler<DeleteNewsCommand, Unit>
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public DeleteNewsCommandHandler(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(DeleteNewsCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.News
                .FirstOrDefaultAsync(c => c.Id == request.Id, cancellationToken);

            if (entity == null)
                throw new DermiException("Wybrany news nie istnieje", "dermi_404", 404);
                   
            _context.News.Remove(entity);
            await _context.SaveDb();

            return Unit.Value;
        }
    }
}