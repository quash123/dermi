using FluentValidation;

namespace Infrastructure.Commands.News.DeleteNews
{
    public class DeleteNewsCommandValidator : AbstractValidator<DeleteNewsCommand>
    {
        public DeleteNewsCommandValidator()
        {
            RuleFor(v => v.Id).NotEmpty();
        }
    }
}