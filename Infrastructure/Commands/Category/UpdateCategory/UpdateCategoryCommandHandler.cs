using MediatR;
using Dermi.Domain.Dtos;
using System.Threading.Tasks;
using System.Threading;
using Dermi.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using Dermi.Domain.Models.Entities;
using AutoMapper;
using Dermi.Domain.Exceptions;
using Infrastructure.Extensions;

namespace Infrastructure.Commands
{
    public class UpdateCategoryCommandHandler : IRequestHandler<UpdateCategoryCommand, CategoryToReturnDto>
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public UpdateCategoryCommandHandler(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<CategoryToReturnDto> Handle(UpdateCategoryCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Categories
                .FirstOrDefaultAsync(c => c.Id == request.Id, cancellationToken);

            if (entity == null)
                throw new DermiException("Kategoria o podanej nazwie nie istnieje", "dermi_404", 404);

            entity = _mapper.Map<Dermi.Domain.Models.Entities.Category>(request);
            await _context.SaveDb();
            
            return _mapper.Map<CategoryToReturnDto>(entity);
        }
    }
}