using Dermi.Domain.Dtos;
using MediatR;

namespace Infrastructure.Commands
{
    public class UpdateCategoryCommand : IRequest<CategoryToReturnDto>
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}