using Dermi.Domain.Dtos;
using MediatR;

namespace Infrastructure.Commands
{
    public class DeleteCategoryCommand : IRequest
    {
        public int Id { get; set; }
    }
}