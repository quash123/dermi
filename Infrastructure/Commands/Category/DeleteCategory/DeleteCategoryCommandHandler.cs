using MediatR;
using Dermi.Domain.Dtos;
using System.Threading.Tasks;
using System.Threading;
using Dermi.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using Dermi.Domain.Models.Entities;
using AutoMapper;
using Dermi.Domain.Exceptions;
using System.Linq;
using Infrastructure.Extensions;

namespace Infrastructure.Commands
{
    public class DeleteCategoryCommandHandler : IRequestHandler<DeleteCategoryCommand, Unit>
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public DeleteCategoryCommandHandler(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(DeleteCategoryCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Categories
                .FirstOrDefaultAsync(c => c.Id == request.Id, cancellationToken);

            if (entity == null)
                throw new DermiException("Kategoria o podanej nazwie nie istnieje", "dermi_404", 404);
            
            if ((await _context.Products.Include(x => x.Category).Where(p => p.Category.Id == request.Id).ToListAsync ()).ToList().Any())
                throw new DermiException("Nie można usunnąć kategorii ponieważ posiada aktywne produkty!", "dermi_400", 400);
            
            _context.Categories.Remove(entity);
            await _context.SaveDb();

            return Unit.Value;
        }
    }
}