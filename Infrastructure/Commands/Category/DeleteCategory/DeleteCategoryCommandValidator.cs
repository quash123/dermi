using FluentValidation;

namespace Infrastructure.Commands.Category.DeleteCategory
{
    public class DeleteCategoryCommandValidator : AbstractValidator<DeleteCategoryCommand>
    {
        public DeleteCategoryCommandValidator()
        {
            RuleFor(v => v.Id).GreaterThan(0);
        }
    }
}