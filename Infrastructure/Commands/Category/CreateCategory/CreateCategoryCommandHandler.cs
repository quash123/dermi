using MediatR;
using Dermi.Domain.Dtos;
using System.Threading.Tasks;
using System.Threading;
using Dermi.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using Dermi.Domain.Models.Entities;
using AutoMapper;
using Dermi.Domain.Exceptions;
using Infrastructure.Extensions;

namespace Infrastructure.Commands
{
    public class CreateCategoryCommandHandler : IRequestHandler<CreateCategoryCommand, CategoryToReturnDto>
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public CreateCategoryCommandHandler(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<CategoryToReturnDto> Handle(CreateCategoryCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Categories
                .FirstOrDefaultAsync(c => c.Name == request.Name, cancellationToken);

            if (entity != null)
                throw new DermiException("Kategoria o podanej nazwie już istnieje", "dermi_400", 400);

            Dermi.Domain.Models.Entities.Category category = new Dermi.Domain.Models.Entities.Category { Name = request.Name};
            _context.Categories.Add(category);
            await _context.SaveDb();

            return _mapper.Map<CategoryToReturnDto>(category);
        }
    }
}