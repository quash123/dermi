using Dermi.Domain.Dtos;
using MediatR;

namespace Infrastructure.Commands
{
    public class CreateCategoryCommand : IRequest<CategoryToReturnDto>
    {
        public string Name { get; set; }
    }
}