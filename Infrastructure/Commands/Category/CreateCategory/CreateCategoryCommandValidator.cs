using FluentValidation;

namespace Infrastructure.Commands.Category.CreateCategory
{
    public class CreateCategoryCommandValidator : AbstractValidator<CreateCategoryCommand>
    {
        public CreateCategoryCommandValidator()
        {
            RuleFor(v => v.Name).NotEmpty();
        }
    }
}