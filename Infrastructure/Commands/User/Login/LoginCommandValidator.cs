using Dermi.Domain.Dtos;
using FluentValidation;
using MediatR;

namespace Infrastructure.Commands
{
    public class LoginCommandValidator : AbstractValidator<LoginCommand>
    {
        public LoginCommandValidator()
        {
            RuleFor(v => v.Password).NotEmpty().WithName("Hasło");
            RuleFor(v => v.UserName).NotEmpty().WithName("Użytkownik");
        }
    }
}