using MediatR;
using Dermi.Domain.Dtos;
using System.Threading.Tasks;
using System.Security.Claims;
using System.Threading;
using Dermi.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using Dermi.Domain.Models.Entities;
using AutoMapper;
using Infrastructure.Support;
using Microsoft.Extensions.Configuration;
using Dermi.Domain.Exceptions;

namespace Infrastructure.Commands
{
    public class LoginCommandHandler : IRequestHandler<LoginCommand, UserToReturnDto>
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;

        public LoginCommandHandler(DataContext context, IMapper mapper, IConfiguration config)
        {
            _context = context;
            _mapper = mapper;
            _config= config;
        }

        public async Task<UserToReturnDto> Handle(LoginCommand request, CancellationToken cancellationToken)
        {
            var user = await _context.Users.FirstOrDefaultAsync (x => x.Username == request.UserName);

            if (user == null)
                throw new DermiException("Nieprawidłowy użytkownik lub hasło", "dermi_401", 401);

            if (!Auth.VerifyPasswordHash (request.Password, user.PasswordHash, user.PasswordSalt))
                throw new DermiException("Nieprawidłowy użytkownik lub hasło", "dermi_401", 401);

            var userDto = _mapper.Map<UserToReturnDto>(user);
            userDto.Token = Auth.GenerateToken(request.UserName, user.Id, _config.GetSection("AppSettings:Token").Value);
            return userDto;
        }
    }
}