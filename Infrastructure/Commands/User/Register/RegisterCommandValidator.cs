using FluentValidation;

namespace Infrastructure.Commands.User.Register
{
    public class RegisterCommandValidator : AbstractValidator<RegisterCommand>
    {
        public RegisterCommandValidator()
        {
            RuleFor(v => v.Password).NotEmpty().WithName("Hasło");
            RuleFor(v => v.UserName).NotEmpty().MinimumLength(6).WithName("Login");
        }
    }
}