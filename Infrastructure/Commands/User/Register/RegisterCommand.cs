using Dermi.Domain.Dtos;
using MediatR;

namespace Infrastructure.Commands
{
    public class RegisterCommand: IRequest<UserToReturnDto>
    {
        
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}