using MediatR;
using Dermi.Domain.Dtos;
using System.Threading.Tasks;
using System.Security.Claims;
using System.Threading;
using Dermi.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using Dermi.Domain.Models.Entities;
using AutoMapper;
using Infrastructure.Support;
using Microsoft.Extensions.Configuration;
using Dermi.Domain.Exceptions;
using System.Linq;
using Infrastructure.Extensions;

namespace Infrastructure.Commands
{
    public class RegisterCommandHandler : IRequestHandler<RegisterCommand, UserToReturnDto>
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;

        public RegisterCommandHandler(DataContext context, IMapper mapper, IConfiguration config)
        {
            _context = context;
            _mapper = mapper;
            _config= config;
        }

        public async Task<UserToReturnDto> Handle(RegisterCommand request, CancellationToken cancellationToken)
        {
            if ((await _context.Users.AnyAsync(x => x.Username == request.UserName)))
                throw new DermiException("Login jest już zajęty", "dermi_400", 400);
         
            Dermi.Domain.Models.Entities.User newUser = new Dermi.Domain.Models.Entities.User();
            Auth.SetUserCreditentials(newUser, request.Password, request.UserName);
            await _context.Users.AddAsync (newUser);
            await _context.SaveDb();
            var userDto = _mapper.Map<UserToReturnDto>(newUser);
            userDto.Token = Auth.GenerateToken(request.UserName, newUser.Id, _config.GetSection("AppSettings:Token").Value);
            return userDto;
        }
    }
}