using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dermi.Domain.Models;
using Dermi.Domain.Models.Entities;
using Infrastructure.Commands;
using Infrastructure.Queries;
using MediatR;
// using Dermi.Repository;
// using Newtonsoft.Json;

namespace Dermi.Infrastructure.Data {
    public class Seed {
        private readonly IMediator _mediator;

        public Seed (IMediator mediator) {
            _mediator = mediator;
        }

        public async Task SeedDb () {
            // if (!_context.Users.Any ()) {
            //     await _authRepo.Register ("a", "a");
            // }
            // if (!_context.Categories.Any ()) {

            //     _dermiRepo.Add (new Category { Name = "Category1" });
            //     _dermiRepo.Add (new Category { Name = "Category2" });
            //     await _dermiRepo.SaveAll ();
            // }

            if (!(await _mediator.Send(new GetCategoriesQuery())).Any()) {
                await _mediator.Send(new CreateCategoryCommand { Name = "Category1"});
                await _mediator.Send(new CreateCategoryCommand { Name = "Category2"});
            }
            
        }

    }
}