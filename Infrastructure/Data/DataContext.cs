using Dermi.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Dermi.Domain.Models.Entities;

namespace Dermi.Infrastructure.Data {
    public class DataContext : DbContext {
        public DataContext (DbContextOptions<DataContext> options) : base (options) { }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<News> News {get; set;}
        
        protected override void OnModelCreating (ModelBuilder builder) {
            builder.Entity<Product> ()
                .HasKey (k => k.Id);
            builder.Entity<Category> ()
                .HasKey (k => k.Id);
            
            builder.Entity<User> ()
                .HasKey (k => k.Id);
            builder.Entity<News> ()
                .HasKey (k => k.Id);
            // builder.Entity<Product> ()
            //     .HasOne (k => k.Category)
            //     .WithMany (u => u.Products)
            //     .HasForeignKey (u => u.Id)
            //     .OnDelete (DeleteBehavior.Restrict);

        }
    }
}