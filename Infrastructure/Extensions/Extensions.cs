using System.Threading.Tasks;
using Dermi.Domain.Exceptions;
using Dermi.Infrastructure.Data;

namespace Infrastructure.Extensions
{
    public static class Extensions
    {
        public static async Task SaveDb(this DataContext context) {
            if (!(await context.SaveChangesAsync () > 0))
            {
                throw new DermiException("Wystąpił błąd w trakcie zapisu do bazy danych!", "dermi_500", 500);
            }
        }
    }
}