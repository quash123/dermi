using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Dermi.Domain.Dtos;
using Dermi.Domain.Models;
using Dermi.Domain.Models.Entities;
using Dermi.Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Queries {
    public class GetNewsQueryHandler : IRequestHandler<GetNewsQuery, PagedList<NewsToReturnDto>> {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        public GetNewsQueryHandler (DataContext context, IMapper mapper) {
            _context = context;
            _mapper = mapper;
        }

        public async Task<PagedList<NewsToReturnDto>> Handle (GetNewsQuery request, CancellationToken cancellationToken) {
            var count = await _context.News.CountAsync();
            var items = await _context.News.Skip((request.PageNumber - 1) * request.PageSize).Take(request.PageSize).ToListAsync();
            var entities = _mapper.Map<List<NewsToReturnDto>>(items);
            return new PagedList<NewsToReturnDto>(entities, count, request.PageNumber, request.PageSize);   
        }
    }
}