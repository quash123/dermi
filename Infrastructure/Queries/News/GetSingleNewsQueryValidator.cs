using System;
using System.Collections.Generic;
using Dermi.Domain.Dtos;
using Dermi.Domain.Models.Entities;
using FluentValidation;
using MediatR;

namespace Infrastructure.Queries.News
{
    public class GetSingleNewsQueryValidator : AbstractValidator<GetSingleNewsQuery>
    {
        public GetSingleNewsQueryValidator()
        {
            RuleFor(v => v.Id).GreaterThan(0);
        }
    }
}