using FluentValidation;

namespace Infrastructure.Queries.Categories
{
    public class GetNewsQueryValidator : AbstractValidator<GetNewsQuery>
    {
        public GetNewsQueryValidator()
        {
            RuleFor(v => v.PageNumber).GreaterThan(0).WithName("Numer Strony");
            RuleFor(v => v.PageSize).GreaterThan(0).LessThanOrEqualTo(50).WithName("Rozmiar Strony");
        }
    }
}