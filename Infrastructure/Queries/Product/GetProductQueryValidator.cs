using FluentValidation;

namespace Infrastructure.Queries.Categories
{
    public class GetProductQueryValidator : AbstractValidator<GetProductQuery>
    {
        public GetProductQueryValidator()
        {
            RuleFor(v => v.Id).GreaterThan(0).WithName("Identyfikator elementu");
        }
    }
}