using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Dermi.Domain.Dtos;
using Dermi.Domain.Exceptions;
using Dermi.Domain.Models.Entities;
using Dermi.Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Queries {
    public class GetProductQueryHandler : IRequestHandler<GetProductQuery, ProductToReturnDto> {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        public GetProductQueryHandler (DataContext context, IMapper mapper) {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ProductToReturnDto> Handle (GetProductQuery request, CancellationToken cancellationToken) {
            var entity = await _context.Products.FirstOrDefaultAsync(p => p.Id == request.Id);
            if (entity == null)
                throw new DermiException("Wybrany produkt nie istnieje", "dermi_404", 404);
            return _mapper.Map<ProductToReturnDto>(entity);
        }
    }
}