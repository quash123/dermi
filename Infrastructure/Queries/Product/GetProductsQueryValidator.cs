using FluentValidation;

namespace Infrastructure.Queries.Products
{
    public class GetProductsQueryValidator : AbstractValidator<GetProductsQuery>
    {
        public GetProductsQueryValidator()
        {
            RuleFor(v => v.PageNumber).GreaterThan(0).WithName("Numer Strony");
            RuleFor(v => v.PageSize).GreaterThan(0).LessThanOrEqualTo(50).WithName("Rozmiar Strony");
        }
    }
}