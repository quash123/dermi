using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Dermi.Domain.Dtos;
using Dermi.Domain.Models;
using Dermi.Domain.Models.Entities;
using Dermi.Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Queries {
    public class GetProductsQueryHandler : IRequestHandler<GetProductsQuery, PagedList<ProductToReturnDto>> {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        public GetProductsQueryHandler (DataContext context, IMapper mapper) {
            _context = context;
            _mapper = mapper;
        }

        public async Task<PagedList<ProductToReturnDto>> Handle (GetProductsQuery request, CancellationToken cancellationToken) {         
            var count = await _context.Products.CountAsync();
            var items = await _context.Products.Skip((request.PageNumber - 1) * request.PageSize).Take(request.PageSize).ToListAsync();
            var entities = _mapper.Map<List<ProductToReturnDto>>(items);
            return new PagedList<ProductToReturnDto>(entities, count, request.PageNumber, request.PageSize);         
        }
    }
}