using System;
using System.Collections.Generic;
using Dermi.Domain.Dtos;
using Dermi.Domain.Models.Entities;
using MediatR;
//using Dermi

namespace Infrastructure.Queries
{
    public class GetCategoryQuery : IRequest<CategoryToReturnDto>
    {
        public int Id {get; set;}
    }
}