using System;
using System.Collections.Generic;
using Dermi.Domain.Dtos;
using Dermi.Domain.Models;
using Dermi.Domain.Models.Entities;
using MediatR;
//using Dermi

namespace Infrastructure.Queries
{
    public class GetCategoriesQuery : IRequest<PagedList<CategoryToReturnDto>>
    {
         public int PageNumber {get; set;} = 1;
         public int PageSize {get; set;} = 10;

    }
}