using FluentValidation;

namespace Infrastructure.Queries.Categories
{
    public class GetCategoriesQueryValidator : AbstractValidator<GetCategoriesQuery>
    {
        public GetCategoriesQueryValidator()
        {
            RuleFor(v => v.PageNumber).GreaterThan(0).WithName("Numer Strony");
            RuleFor(v => v.PageSize).GreaterThan(0).LessThanOrEqualTo(50).WithName("Rozmiar Strony");
        }
    }
}