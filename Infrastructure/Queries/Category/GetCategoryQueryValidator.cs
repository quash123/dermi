using FluentValidation;

namespace Infrastructure.Queries.Categories
{
    public class GetCategoryQueryValidator : AbstractValidator<GetCategoryQuery>
    {
        public GetCategoryQueryValidator()
        {
            RuleFor(v => v.Id).GreaterThan(0).WithName("Identyfikator elementu");
        }
    }
}