using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Dermi.Domain.Dtos;
using Dermi.Domain.Models;
using Dermi.Domain.Models.Entities;
using Dermi.Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Queries {
    public class GetCategoriesQueryHandler : IRequestHandler<GetCategoriesQuery, PagedList<CategoryToReturnDto>> {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        public GetCategoriesQueryHandler (DataContext context, IMapper mapper) {
            _context = context;
            _mapper = mapper;
        }

        public async Task<PagedList<CategoryToReturnDto>> Handle (GetCategoriesQuery request, CancellationToken cancellationToken) {         
            var count = await _context.Categories.CountAsync();
            var items = await _context.Categories.Skip((request.PageNumber - 1) * request.PageSize).Take(request.PageSize).ToListAsync();
            var entities = _mapper.Map<List<CategoryToReturnDto>>(items);
            return new PagedList<CategoryToReturnDto>(entities, count, request.PageNumber, request.PageSize);         
        }
    }
}