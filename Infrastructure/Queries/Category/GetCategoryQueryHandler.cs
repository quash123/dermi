using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Dermi.Domain.Dtos;
using Dermi.Domain.Exceptions;
using Dermi.Domain.Models.Entities;
using Dermi.Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Queries {
    public class GetCategoryQueryHandler : IRequestHandler<GetCategoryQuery, CategoryToReturnDto> {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        public GetCategoryQueryHandler (DataContext context, IMapper mapper) {
            _context = context;
            _mapper = mapper;
        }

        public async Task<CategoryToReturnDto> Handle (GetCategoryQuery request, CancellationToken cancellationToken) {
            var entity = await _context.Categories.FirstOrDefaultAsync(p => p.Id == request.Id);
            if (entity == null)
                throw new DermiException("Wybrana kategoria nie istnieje", "dermi_404", 404);
            return _mapper.Map<CategoryToReturnDto>(entity);
        }
    }
}